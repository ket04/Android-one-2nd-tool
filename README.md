# Android-one-2nd-tool 
![AOT Logo](https://raw.githubusercontent.com/TheDoop/Android-one-2nd-tool/master/%C4%B0mages/daot.ico)

Tool for Android one 2nd generation devices

## features
* İnstall TWRP (seed & crackling)
* Root device (Magisk & SuperSU)
* Oem lock & Unlock
* Log

## Screenshots
![main](https://gitlab.com/doopthe/Android-one-2nd-tool/raw/master/%C4%B0mages/main.PNG)
![root](https://gitlab.com/doopthe/Android-one-2nd-tool/raw/master/%C4%B0mages/root.PNG)

## Usage
Just open Daot.bat

## Next ?
- [ ] APK installer
- [ ] Driver İnstalled
- [x] Root
- [x] TWRP installer
